python.exe -m venv venv
call .\venv\scripts\activate.bat
pip.exe install --upgrade pip
pip.exe install -r requirements.txt

echo 'setup complete.'