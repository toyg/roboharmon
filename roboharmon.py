#!/usr/bin/env python3

# RoboHarmon - Copyright 2019 Giacomo Lacava -
# released under terms of BSD license in LICENSE.txt, also available at
# https://opensource.org/licenses/BSD-3-Clause

"""
Usage:
roboharmon [--audio | --video | --all] [--user <username>] [--from <from_date>]
           [--to <to_date>] [--outdir <output_dir>] [-h | --help]

Options:
    --audio     Audio only (the full podcast)
    --video     Video only (requires Harmontown active subscription).
                This is the default.
    --all       Both audio and video (requires Harmontown active subscription).
                This is done sequentially (all videos first).
    --user <username>       Harmontown username for login. If not provided,
                            you will be challenged for it.
    --from <from_date>      Start downloading episodes published after this date.
                            Date format is YYYY-MM-DD.
                            E.g. to download episodes from 2017 onwards:
                            roborharmon --from 2017-01-01
    --to <from_date>        Only download episodes published before this date.
                            Date format is YYYY-MM-DD.
                            E.g. to download episodes up to 2016 included:
                            roborharmon --to 2016-12-31
    --outdir <output_dir>   Directory that will contain the saved files.
                            If not specified, defaults to the current directory.
    -h, --help              Print this message
"""
import getpass
import json
import re
import sys
from datetime import datetime
from html import unescape
from operator import itemgetter
from os import makedirs
from os.path import abspath, exists, join
from time import sleep, mktime

import bs4
import feedparser
import keyring
import requests
from clint.textui import progress
from docopt import docopt
from fake_useragent import UserAgent

__release__ = "1.1.3"
__label__ = "RoboHarmon"

UA = None
try:
    UA = UserAgent()
except Exception as ex:
    # UserAgent can spit an ugly traceback users don't care about
    pass

# setting keyring explicitly to get around PyInstaller bugs
import keyring.backends
kr = None
if sys.platform == "darwin":
    from keyring.backends import OS_X
    kr = OS_X.Keyring()
if sys.platform == "linux":
    try:
        from keyring.backends import kwallet
        kr = kwallet.KeyringBackend()
    except Exception:
        from keyring.backends import SecretService
        kr = SecretService.KeyringBackend()
if sys.platform.startswith('win'):
    from keyring.backends import Windows
    kr = Windows.WinVaultKeyring()

keyring.set_keyring(kr)

WP_LOGIN = 'https://www.harmontown.com/wp-login.php'
WP_JSON = 'https://www.harmontown.com/wp-json/wp/v2/'
FEED_URL = 'http://feeds.feedburner.com/HarmontownPodcast'
RD_SERVICE = 'roboharmon'
LOGIN_HEADERS = {
    'User-Agent': str(UA.chrome),
    'Referer': WP_LOGIN,
    'Cookie': 'wordpress_test_cookie=WP+Cookie+check'
}
USER_PROMPT = 'Enter your Harmontown username: '
PWD_PROMPT = 'Enter your Harmontown password: '

POST_CATEGORIES = {
    'videos': '8,70',
    'audio': '3'
}

MP4_FILTER = re.compile(".mp4$")
TITLE_FILTER = re.compile(r'[:|<|>|"|/|\||\*|\?|\\|\.]')


def sanitize_title(title):
    out_title = TITLE_FILTER.sub('', unescape(title)).replace('–', '-')
    vps = ['Video Episode: ', 'Video Episode ']
    for vp in vps:
        out_title = out_title.replace(vp, '')
    return out_title


def parse_input_date(date_str):
    return datetime.strptime(date_str, '%Y-%m-%d')


def _is_feedpost_in_range(post, from_date: datetime, to_date: datetime):
    pub_date = datetime.fromtimestamp(mktime(post['published_parsed']))
    return pub_date > from_date and pub_date < to_date


class RoboDan:
    def __init__(self, user=None, target_dir=None):
        self.user = user
        self._logged_on = False
        self.web = requests.Session()
        target_folder = target_dir or '.'
        target_folder = abspath(target_folder)
        if not exists(target_folder):
            try:
                makedirs(target_folder)
            except Exception as e:
                print("Could not create %s: %s" % (target_folder, str(e)))
                target_folder = abspath('.')
                print("Using current directory: %s" % target_folder)
        self.target_dir = target_folder
        self.inventory_path = join(self.target_dir, '.inventory.json')
        self.completed = []
        self._load_inventory()

    # -- BEGIN local utility methods for login and job-tracking

    def _get_password(self):
        # get with side-effects, you scoundrel
        cred = keyring.get_password(RD_SERVICE, self.user)
        if not cred:
            cred = getpass.getpass(PWD_PROMPT)
            print("Saving credentials in a safe place...")
            keyring.set_password(RD_SERVICE, self.user, cred)
        return cred

    def _login(self):
        if self._logged_on:
            return
        self.user = self.user or getpass.getpass(USER_PROMPT)
        if not self.user:
            raise Exception("No valid username provided")

        data = {
            'log': self.user,
            'pwd': self._get_password(),
            'wp-submit': 'Log In',
            'redirect-to': '', 'testcookie': '1'
        }
        print("Logging on...")
        # get initial cookies
        self.web.get(WP_LOGIN, headers=LOGIN_HEADERS)

        # post credentials
        result = self.web.post(WP_LOGIN,
                               data=data,
                               headers=LOGIN_HEADERS,
                               allow_redirects=False)
        if result.status_code != 302:
            self._clear_password()
            self._logged_on = False
            raise Exception("Login failed.")
        self._logged_on = True

    def _clear_password(self):
        keyring.delete_password(RD_SERVICE, self.user)

    def _load_inventory(self):
        print("Loading inventory to track job progress...")
        if exists(self.inventory_path):
            with open(self.inventory_path, "r") as inv_file:
                self.completed = json.load(inv_file)
        else:
            with open(self.inventory_path, "w") as inv_file:
                json.dump(self.completed, inv_file)

    def _save_inventory(self):
        with open(self.inventory_path, 'w') as inv_file:
            json.dump(self.completed, inv_file)

    # -- END utility methods

    def get_posts(self,
                  from_date: datetime = None,
                  to_date: datetime = None,
                  categories=None,
                  offset=0):
        """
        Download post definition via the efficient Wordpress JSON interface.

        :param from_date: datetime to start from
        :param to_date: datetime to end with
        :param categories: type of post, valid values are in POST_CATEGORIES.
                            Note: in practice, non-video categories are a mess.
        :param offset: pagination offset, since the max amount of
                        retrievable posts is 100
        :return: list of dicts {'title': str, 'url': str, 'date': datetime}
        """

        page_size = 100
        category_filter = categories or POST_CATEGORIES['videos']
        if '8' in category_filter:
            self._login()

        after_filter_dt = from_date or datetime(2014, 10, 1)
        before_filter_dt = to_date or datetime(2020, 1, 1)
        parameters = {
            'categories': category_filter,
            'after': after_filter_dt.isoformat(),
            'before': before_filter_dt.isoformat(),
            'per_page': page_size,
            'orderby': 'date',
            'order': 'asc',
            'offset': offset
        }
        result = self.web.get(WP_JSON + 'posts', params=parameters)
        js = result.json()
        posts = [{'title': sanitize_title(p['title']['rendered']),
                  'url': p['link'],
                  'date': datetime.fromisoformat(p['date'])} for p in js]
        if len(posts) == page_size:
            # omg we got more!
            posts.extend(self.get_posts(from_date, to_date, categories,
                                        offset + page_size))
        return posts

    def _extract_video_link(self, post_url):
        """
        Given a web page, find the best video link.
        :param post_url: url to web page
        :return: str with the video url
        """
        sleep(0.5)  # be nice to the server
        post_page = self.web.get(post_url)
        bspage = bs4.BeautifulSoup(post_page.content, features="html.parser")
        links = [tag.get('href') for tag in
                 bspage.find_all('a', title='Download', href=MP4_FILTER)]
        if len(links) == 0:  # no result
            return None
        if len(links) == 1:  # only one version available
            return links[0]
        for l in links:  # choose the HQ version if multiple available
            if l.endswith('-final.mp4'):
                return l
        # still here? weird. Well, pick the first
        print("weird multiple versions:\n" + "\n".join(links))
        return links[0]

    def _save_media_file(self, media_url, file_name):
        """
        Download a media file.
        :param media_url: original url
        :param file_name: file name it will be saved as.
        """
        if media_url in self.completed:
            print("...already downloaded, skipping.")
            return
        target_file = join(self.target_dir, file_name)
        with self.web.get(media_url, stream=True) as media_request:
            with open(target_file, 'wb') as saved_file:
                total_length = int(media_request.headers.get('content-length'))
                for chunk in progress.bar(
                        media_request.iter_content(chunk_size=1024),
                        expected_size=(total_length / 1024) + 1):
                    if chunk:
                        saved_file.write(chunk)
            self.completed.append(media_url)
            self._save_inventory()

    def download_video(self, video_url, file_name):
        """ Download a video file """
        self._save_media_file(video_url, file_name)

    def download_audio(self, audio_url, file_name):
        """ Download an audio file """
        self._save_media_file(audio_url, file_name)

    def download_video_from_post(self, post):
        """ given a post definition, find and download the video.
        :param post: dict of format {'title': str, 'url':str, 'date': datetime}"""
        video_link = self._extract_video_link(post['url'])
        print("Downloading " + video_link + '...')
        out_name = post['date'].strftime('%Y-%m-%d - ') + post['title'] + '.mp4'
        print('Saved as: ' + out_name)
        self.download_video(video_link, out_name)

    def download_videos_in_date_range(self,
                                      from_date: datetime,
                                      to_date: datetime):
        """
        Download all videos published in this range
        :param from_date: start datetime (excluded)
        :param to_date:  end datetime (excluded)
        """
        print("Downloading videos from %s to %s..." % (
            from_date.strftime('%Y-%m-%d'), to_date.strftime('%Y-%m-%d')))
        posts = self.get_posts(from_date, to_date)
        for p in posts:
            self.download_video_from_post(p)
        # one episode is not tagged correctly
        hack_date = parse_input_date("2019-10-15")
        if from_date <= hack_date <= to_date:
            self.download_video_from_post({
                'title': sanitize_title("Nice Dunking, Cool Swishing"),
                'url': "https://www.harmontown.com/2019/10/video-episode-353-nice-dunking-cool-swishing/",
                'date': hack_date})

    def download_audios_in_date_range(self, from_date, to_date):
        """
        Download all audios published in this date range.
        :param from_date: start datetime (excluded)
        :param to_date: end datetime (excluded)
        """
        # sadly, this loses the ep numbers.
        # unfortunately, the website seems a mess for audio posts
        # (all duplicated, some multiple times), so we have to use the feed
        print("Downloading audios from %s to %s..." % (
            from_date.strftime('%Y-%m-%d'), to_date.strftime('%Y-%m-%d')))

        parser = feedparser.parse(FEED_URL)
        posts = [p for p in parser['entries'] if
                 _is_feedpost_in_range(p, from_date, to_date)]
        posts.sort(key=itemgetter('published_parsed'))
        for post in posts:
            dl_link = post.get('links', [{}])[0].get('href')
            if dl_link:
                pub_time = post['published_parsed']
                file_name = "%i-%02d-%02d" % (
                pub_time.tm_year, pub_time.tm_mon, pub_time.tm_mday)
                file_name += " - %s.mp3" % sanitize_title(post['title'])
                print("Downloading " + dl_link + '...')
                print('Saved as: ' + file_name)
                self.download_audio(dl_link, file_name)


if __name__ == '__main__':
    args = docopt(__doc__, version=f"{__label__} {__release__}")

    START_DATE = datetime(2014, 10, 1)  # first ep is in october 2014
    END_DATE = datetime(2019, 12, 31)  # last ep is in december 2019
    try:
        mode = 'video'
        if args['--audio']:
            mode = 'audio'
        if args['--all']:
            mode = 'all'

        from_date = parse_input_date(args['--from']) if args[
            '--from'] else START_DATE
        to_date = parse_input_date(args['--to']) if args[
            '--to'] else END_DATE
        user = args['--user']
        out_dir = args['--outdir'] or '.'
        out_dir = abspath(out_dir)

        rd = RoboDan(user, out_dir)
        # yes, we could have two threads here, but let's be gentle,
        # I don't know if they come from different servers.
        if mode in ['video', 'all']:
            rd.download_videos_in_date_range(from_date, to_date)
        if mode in ['audio', 'all']:
            if from_date == START_DATE:
                # audios started earlier
                from_date = datetime(2012,1,1)
            rd.download_audios_in_date_range(from_date, to_date)
        print(""" 
Job done.
        
        "Nice shootin', son. What's your name?"
        "Murphy."
        """)
    except ValueError as ve:
        # invalid date string passed
        sys.exit('ERROR: ' + str(ve))
    except Exception as e:
        sys.exit('ERROR: ' + str(e))
