call venv\scripts\activate
pyinstaller --clean -y ^
	--hidden-import=win32ctypes ^
	--hidden-import=pywin32 ^
	--hidden-import=win32timezone ^
	-F -i logo.ico ^
	roboharmon.py
deactivate